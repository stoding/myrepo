package algorithms;
// https://leetcode.com/problems/implement-strstr
//Implement strStr().
//Given two strings needle and haystack, return the index of the first
//occurrence of needle in haystack, or -1 if needle is not part of haystack.
//What should we return when needle is an empty string? This is a great question to ask during an interview.
//For the purpose of this problem, we will return 0 when needle is an empty string

public class GetStringOccurrence {

    public static void main(String[] args) {
        GetStringOccurrence get = new GetStringOccurrence();
        System.out.println(get.strStr("sertttsers","sers"));
    }

    public int strStr(String haystack, String needle) {

        int index=-1;
        if(needle.length()==0)
            return 0;
        if(needle.length()>haystack.length())
            return -1;
        for(int i=0;i<haystack.length();i++){
            if(haystack.charAt(i)==needle.charAt(0)){
                index = i;
                   try {
                       for (int j =0 ; j<needle.length();j++){
                           if(haystack.charAt(i+j)==needle.charAt(j)) {
                               if(j==needle.length()-1)
                                   return index;
                           }
                           else {
                               index = -1;
                               break;
                           }
                       }
                   }
                   catch (IndexOutOfBoundsException e){
                       return -1;
                   }
            }
        }
        return index;
    }
}
