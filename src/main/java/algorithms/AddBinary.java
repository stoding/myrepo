package algorithms;

// Given two binary strings a and b, return their sum as a binary string.

public class AddBinary {
    public static void main(String[] args) {

        String str1 = "10100000100100110110010000010101111011011001101110111111111101000000101111001110001111100001101";
        String str2 = "110101001011101110001111100110001010100001101011101010000011011011001011101111001100000011011110011";
        AddBinary add = new AddBinary();
        System.out.println(add.addBinary(str1, str2));

    }

    public String addBinary(String a, String b) {
        AddBinary add = new AddBinary();
        if (a.length() >= b.length())
            return add.binSum(a, b).toString();
        else return add.binSum(b, a).toString();

    }

    public StringBuilder binSum(String str1, String str2) {
        StringBuilder total = new StringBuilder();
        int sum;
        int overDigit = 0;

        for (int i = 1; i < str2.length() + 1; i++) {
            sum = Character.getNumericValue(str2.charAt(str2.length() - i)) + Character.getNumericValue(str1.charAt(str1.length() - i));
            switch (sum + overDigit) {
                case 0:
                    total.insert(0, "0");
                    continue;
                case 1: {
                    total.insert(0, "1");
                    if (overDigit == 1)
                        overDigit = 0;
                    continue;
                }
                case 2: {
                    total.insert(0, "0");
                    overDigit = 1;
                    continue;
                }

                case 3: {
                    total.insert(0, "1");
                    overDigit = 1;
                }
            }
        }
        for (int i = str1.length() - str2.length() - 1; i >= 0; i--) {
            if (Character.getNumericValue(str1.charAt(i)) + overDigit == 2) {
                total.insert(0, "0");
                continue;
            }
            if (overDigit == 1) {
                total.insert(0, "1");
                overDigit = 0;
            } else total.insert(0, str1.charAt(i));
        }
        if (overDigit == 1)
            total.insert(0, "1");

        return total;
    }
}
