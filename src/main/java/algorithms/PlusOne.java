package algorithms;

// You are given a large integer represented as an integer array digits,
// where each digits[i] is the ith digit of the integer. The digits are
// ordered from most significant to least significant in left-to-right order.
// The large integer does not contain any leading 0's.
//
// Increment the large integer by one and return the resulting array of digits.

import java.util.Arrays;

public class PlusOne {

    public static void main(String[] args) {
        int[] array = {8,9,9,9};
        PlusOne plusOne = new PlusOne();

        array = plusOne.plusOne(array);
        System.out.println(Arrays.toString(array));
    }

    public int[] plusOne(int[] digits) {
        int n = digits.length;
        for(int i=n-1;i>=0;i--)
        {
            if(digits[i] == 9)
                digits[i] = 0;
            else
            {
                digits[i]++;
                return digits;
            }
        }

        digits = new int[n+1];
        digits[0] = 1;
        return digits;
    }

}
