package algorithms;

//Given a sorted array of distinct integers and a target value, return the
// index if the target is found. If not, return the index where it would be
// if it were inserted in order.
//
//You must write an algorithm with O(log n) runtime complexity.

public class SearchInsertPosition {
    public static void main(String[] args) {
        SearchInsertPosition search = new SearchInsertPosition();
        int[] array = {1,3};
        System.out.println(search.searchInsert(array,3));
    }

    public int searchInsert(int[] nums, int target) {
        if (target <= nums[0])
            return 0;

        if (target>nums[nums.length-1])
            return nums.length;

        for(int i=0;i<nums.length;i++) {

            if(target<=nums[i])
                return i;

        }
        return nums.length;
    }

}
