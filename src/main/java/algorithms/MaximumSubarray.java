package algorithms;

// Given an integer array nums, find the contiguous subarray
// (containing at least one number) which has the largest sum and return its sum.
//
// A subarray is a contiguous part of an array.

public class MaximumSubarray {
    public static void main(String[] args) {
        int[] array = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        MaximumSubarray max = new MaximumSubarray();
        System.out.println(max.maxSubArray(array));

    }

    public int maxSubArray(int[] nums) {
        int result = nums[0];
        int sum = 0;

        boolean isPositive = false;
        if (nums[0] >= 0)
            isPositive = true;
        if (nums.length == 1)
            return nums[0];


        for (int i = 0; i < nums.length; i++) {
            if (!isPositive) {
                if (nums[i] >= 0) {
                    result = nums[i];
                    sum = nums[i];
                    isPositive = true;
                    continue;
                }
                if (nums[i] > result)
                    result = nums[i];
            }
            if (isPositive) {
                if (nums[i] >= 0) {
                    sum += nums[i];
                    if (sum > result) {
                        result = sum;
                    }
                }
                if (nums[i] < 0) {
                    if (sum > result) {
                        result = sum;
                    }
                    if ((sum + nums[i]) < 0) {
                        sum = 0;
                    }
                    if ((sum + nums[i]) >= 0) {
                        sum += nums[i];
                    }

                }
            }
        }
        return result;
    }
}
