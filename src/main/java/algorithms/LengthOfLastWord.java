package algorithms;

//Given a string s consisting of some words separated by some number
//of spaces, return the length of the last word in the string.
//
//A word is a maximal substring consisting of non-space characters only.

public class LengthOfLastWord {

    public static void main(String[] args) {
        LengthOfLastWord length = new LengthOfLastWord();
        System.out.println(length.lengthOfLastWord("s"));
    }
    public int lengthOfLastWord(String s) {
        int counter = 0;
        for (int i=s.length()-1;i>=0;i--){
            if(s.charAt(i)==' ')
                continue;
            while (i>=0 && s.charAt(i)!=' ') {
                counter++;
                i--;
            }
            break;
        }
        return counter;
    }
}
