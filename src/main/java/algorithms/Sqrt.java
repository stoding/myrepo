package algorithms;

// Given a non-negative integer x, compute and return the square root of x.
//
// Since the return type is an integer, the decimal digits are truncated,
// and only the integer part of the result is returned.
//
// Note: You are not allowed to use any built-in exponent function or operator,
// such as pow(x, 0.5) or x ** 0.5.
//


public class Sqrt {

    public static void main(String[] args) {
        Sqrt result = new Sqrt();
        System.out.println(result.mySqrt(2147483647));
    }

    public int mySqrt(int x) {
        if (x == 0) return 0;
        int left = 1;
        int right = x / 2 + 1;
        long mid = 0;

        while (left < (right - 1)) {
            mid = (left + right) / 2;
            if (mid * mid == x)
                return (int) mid;
            if (mid * mid > x)
                right = (int) mid;
            else left = (int) mid;

        }
        return left;
    }
}


